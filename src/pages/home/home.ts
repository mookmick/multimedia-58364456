import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
declare var google;
import { Plugins } from '@capacitor/core'
const { Geolocation } = Plugins;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  isQr:boolean=false;
  isPay:boolean=false;
  constructor(public navCtrl: NavController) {

  }

  login(){
    console.log("login");
  }

  ionViewDidLoad(){
    this.loadMap();
  }


  loadMap(){
    Geolocation.getCurrentPosition().then(data => {
      let latLng = { lat: data.coords.latitude, lng: data.coords.longitude }
      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      var iconok = {
        url: 'assets/imgs/placeholder.png',
        scaledSize: new google.maps.Size(30, 30),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 30)
      };
      var marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: iconok
      });
      this.directionsDisplay.setMap(this.map);

      var service = new google.maps.places.PlacesService(this.map);
      service.nearbySearch({
        location: latLng,
        radius: 1000,
        type: ['shop']
      }, (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            this.createMarker(results[i], this.map);
            // console.log(results[i])
          }
        }
      });
    }).catch(err => {

    })
  }
  createMarker(place, map) {
    var iconok = {
      url: 'assets/imgs/shopping.png',
      scaledSize: new google.maps.Size(30, 30),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 30)
    };
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: this.map,
      position: placeLoc,
      icon: iconok
    });

    google.maps.event.addListener(marker, 'click', ()=>{
      // this.navCtrl.push(HomePage)
      // window.open(map, marker);
    });
  }


  qr(){
    this.isQr=!this.isQr;
  }
  pay(){
    this.isPay=!this.isPay;
  }
  logout(){
    this.navCtrl.pop();
  }
}
